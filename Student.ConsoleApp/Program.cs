﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Создание базы данных
            DataSet studentsDB = new DataSet("StudentsDB");
            // 2. Создание таблиц
            DataTable students = new DataTable("Students");
            DataTable gender = new DataTable("Gender");
            // 3. Создание колонок (столбцов)
            InitStudentTable(ref students);
            InitGenderTable(ref gender);
            // 4. Добавление таблицы в базу данных
            //studentsDB.Tables.Add(students); // одиночное добавление таблицы
            // 5. Создание внешнего ключа для связи таблиц
            ForeignKeyConstraint FK_Gender = new ForeignKeyConstraint(gender.Columns["Id"], students.Columns["GenderId"])
            {
                // ограничения для внешнего ключа на удаление
                DeleteRule = Rule.Cascade,
                UpdateRule = Rule.Cascade
            };
            // 6. Добавление внешнего ключа в таблицу students
            students.Constraints.Add(FK_Gender);

            // 7.
            studentsDB.Tables.AddRange(new DataTable[] // массовое добавление таблиц
            {
                 gender,
                 students // students ссылается на внешний ключ от таблицы gender, поэтому идет после нее
            });

            #region Write record
            // Создание записей в таблице и сохранение в xml формате
            DataRow newRow = gender.NewRow();
            newRow["StudentGender"] = "Мужской";
            gender.Rows.Add(newRow);

            newRow = gender.NewRow();
            newRow["StudentGender"] = "Женский";
            gender.Rows.Add(newRow);
            gender.WriteXml("gender.xml");
            #endregion

            void AddRows(DataTable stud)
            {

                DataRow addNewRow = stud.NewRow();
                Console.WriteLine("FIO");
                addNewRow["FIO"] = Console.ReadLine();
                //Console.WriteLine("Gender");


                int studGendId;

                //addNewRow["GenderId"] = Console.ReadLine();


                bool foo = true;
                while(foo)
                {
                    Console.WriteLine("Gender");
                    studGendId = Console.Read();
                    if (studGendId < 0 || studGendId > 3)
                    {
                        addNewRow["GenderId"] = studGendId;
                        foo = false;
                    }
                    else
                    {
                        foo = true;
                        
                    }
                }

                stud.Rows.Add(addNewRow);
                stud.WriteXml("stud.xml");
            }

            AddRows(students);
        }

        private static void InitGenderTable(ref DataTable gender)
        {
            DataColumn id = new DataColumn("Id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Caption = "Идентификатор";
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn studentGender = new DataColumn("StudentGender", typeof(string))
            {
                Caption = "Пол",
                AllowDBNull = false,
                MaxLength = 10,
            };

            gender.Columns.AddRange(new DataColumn[]
            {
                id,
                studentGender
            });

            gender.PrimaryKey = new DataColumn[] { gender.Columns["Id"] };
            
        }

        private static void InitStudentTable(ref DataTable students)
        {
            DataColumn id = new DataColumn("Id", typeof(int));
            id.AllowDBNull = false;
            id.AutoIncrement = true;
            id.AutoIncrementSeed = 1;
            id.AutoIncrementStep = 1;
            id.Caption = "Идентификатор";
            id.Unique = true;
            id.ReadOnly = true;

            DataColumn fio = new DataColumn("FIO", typeof(string));
            fio.Caption = "ФИО студента";
            fio.MaxLength = 60;

            DataColumn genderId = new DataColumn("GenderId", typeof(int))
            {
                Caption = "Пол",
                AllowDBNull = false
            };

            // Добавление (два за раз) столбцов в таблицу
            students.Columns.AddRange(new DataColumn[]
            {
                id,
                fio,
                genderId
            });

            //students.PrimaryKey = new DataColumn[]{ students.Columns[0]}; //если уверены что столбец первый. или указываем по имени столбца
            students.PrimaryKey = new DataColumn[] { students.Columns["Id"] };
        }
    }
}
